package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.CreateDeliveryDO;
import com.tbs.generic.vansales.Model.CreateDeliveryMainDO;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CreatePurchaseRecieptRequest extends AsyncTask<String, Void, Boolean> {

    private CreateDeliveryMainDO createDeliveryMainDO;
    private CreateDeliveryDO createDeliveryDO;
    private Context mContext;
    private String name, site, location;
    ArrayList<ActiveDeliveryDO> purchaseRecieptDos;
    String username, password, ip, pool, port, lattitude, longitude, address,signature;
    PreferenceUtils preferenceUtils;
    static int networkTimeOut=60*1000;
    String specificPurchaseReciept;
    String message = "";

    public CreatePurchaseRecieptRequest(String lattitudE, String longitudE, String addresS,String locatioN,String purchaseReciept,String customer, String site, ArrayList<ActiveDeliveryDO> purchaseRecieptDOs, Context mContext) {

        this.mContext = mContext;
        this.name = customer;
        this.site = site;
        this.location=locatioN;
        this.specificPurchaseReciept=purchaseReciept;
        this.purchaseRecieptDos = purchaseRecieptDOs;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }
    public void signature(@Nullable String signature) {
        this.signature = signature;
    }
    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, CreateDeliveryMainDO createDeliveryMainDO,String msg);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String site     = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");
        String role = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        String nonSheduledId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
        String vrID = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "");
        PodDo podDo = StorageManager.getInstance(mContext).getDepartureData(mContext);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_XSTKVCR", nonSheduledId);
            jsonObject.put("I_XVCRNUM", vrID);
            jsonObject.put("I_XCREUSR", role);
//            jsonObject.put("I_XNOP", podDo.getName());

            jsonObject.put("I_XPRHFCY", site);
            jsonObject.put("I_XBPSNUM", name);
            jsonObject.put("I_XPTHNUM", specificPurchaseReciept);
            jsonObject.put("I_XVEHCODE", location);
            if (!TextUtils.isEmpty(signature)) {
                jsonObject.put("I_XSIGNATURE", signature);
            }
            JSONArray jsonArray = new JSONArray();
            if (purchaseRecieptDos != null && purchaseRecieptDos.size() > 0) {
                for (int i = 0; i < purchaseRecieptDos.size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("I_XITMREF", purchaseRecieptDos.get(i).product);
                    jsonObject1.put("I_XQTYUOM", purchaseRecieptDos.get(i).orderedQuantity);
                    jsonObject1.put("I_XSTA", purchaseRecieptDos.get(i).purchaseOrderStatus);
                    jsonObject1.put("I_XPRICE", purchaseRecieptDos.get(i).price);
                    jsonObject1.put("I_XLOT", purchaseRecieptDos.get(i).purchaseOrderLotNumber);
                    jsonObject1.put("I_XBPSLOT", purchaseRecieptDos.get(i).purchaseOrderSupplierLOT);
                    jsonObject1.put("I_XSERNUM", purchaseRecieptDos.get(i).purchaseOrderSerialNumber);

                    jsonArray.put(i, jsonObject1);
                }
                jsonObject.put("GRP2", jsonArray);
            }
            JSONArray jsonArray1 = new JSONArray();
            if (podDo.getCapturedImagesListBulk() != null && podDo.getCapturedImagesListBulk().size() > 0) {
                for (int i = 0; i < podDo.getCapturedImagesListBulk().size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("I_XIMAGE", podDo.getCapturedImagesListBulk().get(i));

                    jsonArray1.put(i, jsonObject1);
                }
                jsonObject.put("GRP3", jsonArray1);
            }
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.CREATE_PURCHASE_RECIEPT, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("create delivery xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            createDeliveryMainDO = new CreateDeliveryMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        createDeliveryMainDO.createDeliveryDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        createDeliveryDO = new CreateDeliveryDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_XPTHNUM")) {
                            if(text.length()>0){
                                createDeliveryMainDO.recieptNumber = text;

                            }


                        }

                        else if (attribute.equalsIgnoreCase("O_XMESSAGE")) {
                            if(text.length()>0){
                                createDeliveryMainDO.message = text;

                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_XFLG")) {
                            if(text.length()>0){
                                createDeliveryMainDO.status = Integer.parseInt(text);

                            }


                        }

                        text="";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        createDeliveryMainDO.createDeliveryDOS.add(createDeliveryDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, createDeliveryMainDO,ServiceURLS.message);

        }
    }
}