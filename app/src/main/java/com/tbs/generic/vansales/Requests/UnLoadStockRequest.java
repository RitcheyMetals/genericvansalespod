package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.Model.LoadStockMainDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.CalendarUtils;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class UnLoadStockRequest extends AsyncTask<String, Void, Boolean> {

    private LoadStockMainDO loanReturnMainDO;
    private LoadStockDO loanReturnDO;
    private Context mContext;
    private String recievedSite,date,customerSite;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    LinkedHashMap<String, ArrayList<LoadStockDO>> returnDOs;
    public UnLoadStockRequest(String recieveSite, String datE, String site, LinkedHashMap<String, ArrayList<LoadStockDO>> returndos, Context mContext) {

        this.recievedSite = recieveSite;
        this.date = datE;
        this.customerSite = site;
        this.returnDOs = returndos;

        this.mContext = mContext;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, LoadStockMainDO loanReturnMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String site  = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");
        String code = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");
        String date =CalendarUtils.getDate();
        String nId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
        String loc = preferenceUtils.getStringFromPreference(PreferenceUtils.LOCATION, "");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("XSRC", nId);
            jsonObject.put("XSFCY", site);
            jsonObject.put("XSDAT", date);
            jsonObject.put("XSLOCD", loc);
            jsonObject.put("XSLOC", code);

            JSONArray jsonArray =new JSONArray();
            ArrayList<String> keySet = new ArrayList<String>(returnDOs.keySet());
            for (int q=0; q<returnDOs.size(); q++) {

                ArrayList<LoadStockDO> loanReturnDos = returnDOs.get(keySet.get(q));
                if(loanReturnDos!=null && loanReturnDos.size()>0) {
                    for (int i = 0; i < loanReturnDos.size(); i++) {
                        JSONObject jsonObject1 = new JSONObject();
                        jsonObject1.put("XSITM", loanReturnDos.get(i).product);
                        jsonObject1.put("XSQTY",  loanReturnDos.get(i).quantity);
                        jsonArray.put(i, jsonObject1);
                    }
                    jsonObject.put("GRP2", jsonArray);
                }

            }


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.UNLOAD_STOCK, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            loanReturnMainDO = new LoadStockMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        loanReturnMainDO.loadStockDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        loanReturnDO = new LoadStockDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("XSTATUS")) {
                            if(text.length()>0){
                                loanReturnMainDO.status = Integer.parseInt(text);

                            }



                        }

                        text="";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        loanReturnMainDO.loadStockDOS.add(loanReturnDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        if (onResultListener != null) {
            onResultListener.onCompleted(!result, loanReturnMainDO);
        }
    }
}