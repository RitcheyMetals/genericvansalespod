package com.tbs.generic.vansales.Model;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;

public class UpdateSerialListRequest extends AsyncTask<String, Void, Boolean> {
  int  linenumber;
    private SerialListDO successDO;
    private Context mContext;
    private ArrayList<SerialListDO> serialListDOS;
    ActiveDeliveryDO trailerSelectionDO;
    PreferenceUtils preferenceUtils;

    public UpdateSerialListRequest(ActiveDeliveryDO shipment, Context mContext) {

        this.mContext = mContext;
        this.trailerSelectionDO = shipment;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, ArrayList<SerialListDO> successDO);

    }

    public boolean runRequest() {
        JSONObject jsonObject = new JSONObject();
        String id = ((BaseActivity)mContext).preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");

        try {
            jsonObject.put("I_YVEHROU", id);
            jsonObject.put("I_YITMREF", trailerSelectionDO.product);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.UPDATE_SERIAL, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        serialListDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        successDO = new SerialListDO();

                    }

                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_YSERNUM")) {
                            if(!text.isEmpty()){
                                successDO.serialDO = text;

                            }


                        }
                        else   if (attribute.equalsIgnoreCase("O_YSELECT")) {
                            if(!text.isEmpty()){
                                successDO.state = Integer.parseInt(text);

                            }


                        }

                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        serialListDOS.add(successDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, serialListDOS);
        }
    }
}