package com.tbs.generic.pod.Activitys

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ScrollView
import com.tbs.generic.vansales.Activitys.ActiveDeliveryActivity
import com.tbs.generic.vansales.Activitys.BaseActivity
import com.tbs.generic.vansales.Activitys.TransactionList
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.include_toolbar.*


class ScheduledSalesActivity : BaseActivity() {
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 200


    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.sheduled_sales_layout, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        flToolbar.visibility = View.GONE
      /*  toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
//            overridePendingTransition(R.anim.exit, R.anim.enter)
        }*/

        tv_title.text= getString(R.string.schedule_sales)
        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
        initializeControls()

    }


    override fun initializeControls() {

        tvScreenTitle.text =getString(R.string.schedule_sales)
        var llDeliveryList = findViewById<View>(R.id.llDeliveryList) as LinearLayout
        var llActiveDelivery = findViewById<View>(R.id.llActiveDelivery) as LinearLayout
        llDeliveryList.setOnClickListener {
            val vehicleCheckOutDo = StorageManager.getInstance(this@ScheduledSalesActivity).getVehicleCheckOutData(this@ScheduledSalesActivity)
            if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                val vehicleCheckInDo = StorageManager.getInstance(this@ScheduledSalesActivity).getVehicleCheckInData(this@ScheduledSalesActivity)
                if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                    val customerDo = StorageManager.getInstance(this@ScheduledSalesActivity).getCurrentSpotSalesCustomer(this@ScheduledSalesActivity)
                    if (customerDo.customer.equals("")) {
                        val vehicleRoutId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
                        if (vehicleRoutId.length > 0) {
                            val intent = Intent(this@ScheduledSalesActivity, TransactionList::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            startActivity(intent)
                        } else {
                            showAppCompatAlert("Alert!", "No Shipments Found", "OK", "", "FAILURE", false)

                        }

                    } else {
                        showAppCompatAlert("", "Please process the current customer " + customerDo.customer + "'s non schedule shipment", "OK", "", "FAILURE", false)
                    }
                } else {
                    showAppCompatAlert("", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                }
            } else {
                showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
            }
        }
        llActiveDelivery.setOnClickListener {
            val vehicleCheckOutDo = StorageManager.getInstance(this@ScheduledSalesActivity).getVehicleCheckOutData(this@ScheduledSalesActivity)
            if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                val vehicleCheckInDo = StorageManager.getInstance(this@ScheduledSalesActivity).getVehicleCheckInData(this@ScheduledSalesActivity)
                if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                    val shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                    if (!shipmentId.isEmpty()) {
//                        val pickupDropFlag = preferenceUtils.getIntFromPreference(PreferenceUtils.PICKUP_DROP_FLAG, 0)
//                        if (pickupDropFlag == 2) {
//
//
//                            val intent = Intent(this@ScheduledSalesActivity, ActiveDeliveryActivity::class.java)
//                            intent.putExtra("SHIPMENT_ID", shipmentId);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                            startActivity(intent)
//                        } else {
//
//
//                            val intent = Intent(this@ScheduledSalesActivity, ActiveDeliveryActivity::class.java)
//                            intent.putExtra("SHIPMENT_ID", shipmentId);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                            startActivity(intent)
//                        }
                        val intent = Intent(this@ScheduledSalesActivity, ActiveDeliveryActivity::class.java)
                        intent.putExtra("SHIPMENT_ID", shipmentId)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(intent)

                    } else {
                        showAppCompatAlert("", "There is No active delivery", "OK", "", "", false)
                    }
                } else {
                    showAppCompatAlert("", "Please Finish Check-in Process", "OK", "", "", false)
                }
            } else {
                showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
            }
        }


    }

    override fun onResume() {
        super.onResume()
    }


}