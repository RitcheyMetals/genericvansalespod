package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class CustomerDo implements Serializable {

    public String customer = "";
    public String customerName        = "";
    public String postBox             = "";
    public String town                = "";
    public String landmark            = "";
    public String countryName         = "";
    public String city                = "";
    public String webSite             = "";
    public String postalCode          = "";
    public String landline            = "";
    public String currency            = "";
    public String mobile              = "";
    public String email               = "";
    public String fax                 = "";
    public String lattitude           = "";
    public String longitude           = "";
    public String isDelivered         = "";
    public int flag                   = 0;
    public String amount              = "";
    public String businessLine        = "";
    public String emirates            = "";
    public boolean isSelected;
    public String deliveryEmail       = "";
    public String invoiceEmail        = "";
    public String paymentEmail        = "";
    public String cylinderIssueEmail  = "";


    public void setSelected(boolean selected) {
        isSelected = selected;
    }
    public boolean isCreditAvailable = true;


    public void customerName(String text) {
        this.customerName = text;
    }

    public void customerId(String text) {
        this.customer = text;
    }

    private boolean isChecked;

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
