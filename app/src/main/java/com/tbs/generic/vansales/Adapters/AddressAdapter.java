package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.ActiveDeliveryActivity;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.CustomerDetailsActivity;
import com.tbs.generic.vansales.Model.AddressDO;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder> {
    int count = 0;
    private ArrayList<AddressDO> addressMainDos;
    private String codE, customeR;
    private Context context;
    private CustomerDo customerDO;
    int type;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvInvoiceNumber;
        public RelativeLayout rlRemove, rlAdd;
        public ImageView ivRemove, ivAdd;
        public TextView tvCustomerName, tvCustomerCode, tvBuisinessLine, tvAddressCode, tvDescription, tvCity, tvEmirates;

        public MyViewHolder(View view) {
            super(view);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvCustomerCode = itemView.findViewById(R.id.tvCustomerCode);
            tvBuisinessLine = itemView.findViewById(R.id.tvBuisinessLine);
            tvAddressCode = itemView.findViewById(R.id.tvAddressCode);

        }
    }


    public AddressAdapter(Context context,int type, ArrayList<AddressDO> invoiceDOS, String code, String customer,CustomerDo customerDo) {
        this.context = context;
        this.addressMainDos = invoiceDOS;
        this.codE = code;
        this.customeR = customer;
        this.customerDO = customerDo;
        this.type = type;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final AddressDO invoiceHistoryDO = addressMainDos.get(position);

//        holder.tvInvoiceNumber.setText(context.getString(R.string.address_code)+" : " + invoiceHistoryDO.addressCode + "\n" +
//                context.getString(R.string.description)+": " + invoiceHistoryDO.addressDescription + "\n" +
//                context.getString(R.string.city)+": " + invoiceHistoryDO.city
//                + "\n" +context.getString(R.string.emirates)+ " : " + invoiceHistoryDO.emirates);

        holder.tvCustomerCode.setText(invoiceHistoryDO.addressCode );
        holder.tvCustomerName.setText(invoiceHistoryDO.addressDescription);
        holder.tvBuisinessLine.setText( invoiceHistoryDO.city);
        holder.tvAddressCode.setText(invoiceHistoryDO.emirates);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (codE.equals("MASTER")) {
                    Intent intent = new Intent(context, CustomerDetailsActivity.class);
                    intent.putExtra("A_CODE", invoiceHistoryDO.addressCode);
                    intent.putExtra("C_CODE", customeR);
                    intent.putExtra("TYPE", type);

                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);

                } else {



                    CustomerDo custDo = StorageManager.getInstance(context).getCurrentSpotSalesCustomer(context);
                    if (custDo != null && !custDo.customer.equalsIgnoreCase("")) {
                        if (custDo.customer.equalsIgnoreCase(customeR)) {
                            //
                            Intent intent = new Intent(context, ActiveDeliveryActivity.class);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_non_scheduled));
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_CODE, invoiceHistoryDO.addressCode);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.SPOT_DOCUMENT, invoiceHistoryDO.addressCode);

                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_DESCRIPTION, invoiceHistoryDO.addressDescription);
//                 context.startActivity(intent);
                            ((BaseActivity) context).startActivityForResult(intent, 63);
                        } else {
                            ((BaseActivity) context).showAppCompatAlert("", "Please process the current customer " + custDo.customer + "'s non schedule shipment", context.getString(R.string.ok), "", "", false);
                        }
                    } else {

                        StorageManager.getInstance(context).deleteActiveDeliveryMainDo(((BaseActivity) context));
                        StorageManager.getInstance(context).deleteDepartureData(((BaseActivity) context));
                        ((BaseActivity) context).preferenceUtils.removeFromPreference(PreferenceUtils.CustomerId);
                        StorageManager.getInstance(context).deleteCurrentSpotSalesCustomer(context);
                        StorageManager.getInstance(context).deleteCurrentDeliveryItems(context);
                        StorageManager.getInstance(context).deleteReturnCylinders(context);

                        if (StorageManager.getInstance(context).saveCurrentSpotSalesCustomer(((BaseActivity) context), customerDO)) {
                            Intent intent = new Intent(context, ActiveDeliveryActivity.class);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_non_scheduled));
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.SPOT_DOCUMENT, invoiceHistoryDO.addressCode);

                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_CODE, invoiceHistoryDO.addressCode);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_DESCRIPTION, invoiceHistoryDO.addressDescription);
                            ((BaseActivity) context).startActivityForResult(intent, 63);

                        }

                    }


                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return addressMainDos != null ? addressMainDos.size() : 0;
    }

}
