package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.alert.rajdialogs.ProgressDialog;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.Model.LoadStockMainDO;
import com.tbs.generic.vansales.Model.SerialListDO;
import com.tbs.generic.vansales.Model.StockItemDo;
import com.tbs.generic.vansales.Model.TrailerSelectionDO;
import com.tbs.generic.vansales.Model.VanStockDetailsDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressDialogTask;
import com.tbs.generic.vansales.utils.ProgressTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.Util;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class VanStockRequest extends AsyncTask<String, Void, Boolean> {

    private VanStockDetailsDO vanStockDetailsDO;
    private StockItemDo stockItemDo;
    private Context mContext;
    PreferenceUtils preferenceUtils;
    private SerialListDO serialListDO;

    public VanStockRequest(Context mContext) {
        this.mContext = mContext;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, VanStockDetailsDO vanStockDetailsDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put(Constants.REQUST_SCHEDULE_ID, preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, ""));
            jsonObject.put(Constants.REQUST_NON_SCHEDULE_ID, preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, ""));

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.SERVICE_LOAD_STOCK, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            vanStockDetailsDO = new VanStockDetailsDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {

                if (eventType == XmlPullParser.START_TAG) {
                    startTag = xpp.getName();
                    attribute = xpp.getAttributeValue(null, "ID");
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP2")) {
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP2");
                        vanStockDetailsDO.stockItemList = new ArrayList<>();
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP3")) {
                        vanStockDetailsDO.serialListDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP3");
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP2")) {
                        stockItemDo = new StockItemDo();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP3")) {
                        serialListDO = new SerialListDO();
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD") && !TextUtils.isEmpty(text)) {

                        //Header Setup
                        if (attribute.equalsIgnoreCase(Constants.TOTAL_SCHEDULE_STOCK_QTY)) {
                            vanStockDetailsDO.totalScheduledStockQty = text;


                        } else if (attribute.equalsIgnoreCase(Constants.TOTAL_SCHEDULE_STOCK_VOLUME)) {
                            vanStockDetailsDO.totalScheduledStockVoulme = text;


                        } else if (attribute.equalsIgnoreCase(Constants.TOTAL_SCHEDULE_STOCK_UNITS)) {
                            vanStockDetailsDO.totalScheduledStockUnits = text;


                        } else if (attribute.equalsIgnoreCase(Constants.TOTAL_UN_SCHEDULE_STOCK_QTY)) {
                            vanStockDetailsDO.totalNonScheduledStockQty = text;


                        } else if (attribute.equalsIgnoreCase(Constants.TOTAL_UN_SCHEDULE_STOCK_VOLUME)) {
                            vanStockDetailsDO.totalNonScheduledStockVoulme = text;


                        } else if (attribute.equalsIgnoreCase(Constants.TOTAL_UN_SCHEDULE_STOCK_UNITS)) {
                            vanStockDetailsDO.totalNonScheduledStockUnits = text;


                        } else if (attribute.equalsIgnoreCase(Constants.VEHICEL_CAPACITY)) {
                            vanStockDetailsDO.vehicleCapacity = text;


                        } else if (attribute.equalsIgnoreCase(Constants.VEHICLE_CAPACITY_UNITS)) {
                            vanStockDetailsDO.vehicleCapacityUnit = text;
                        } else if (attribute.equalsIgnoreCase(Constants.VEHICLE_VOLUME)) {
                            vanStockDetailsDO.vehicleVolume = text;


                        } else if (attribute.equalsIgnoreCase(Constants.VEHICLE_VOLUME_UNITS)) {
                            vanStockDetailsDO.vehicleVolumeUnit = text;


                        } else if (attribute.equalsIgnoreCase(Constants.O_XPICKUPSTK)) {
                            vanStockDetailsDO.pickUpStock = text;


                        } else if (attribute.equalsIgnoreCase(Constants.O_XDROPSTK)) {
                            vanStockDetailsDO.dropStock = text;


                        }

                        //Items Setup
                        else if (attribute.equalsIgnoreCase(Constants.SHIPMENT_NUMBER)) {
                            stockItemDo.shipmentNumber = text;


                        } else if (attribute.equalsIgnoreCase(Constants.STOCK_TYPE)) {
                            stockItemDo.stockType = Integer.parseInt(text);


                        } else if (attribute.equalsIgnoreCase(Constants.PRODUCT_NAME)) {
                            stockItemDo.productName = text;


                        } else if (attribute.equalsIgnoreCase(Constants.PRODUCT_DESC)) {
                            stockItemDo.productDescription = text;
                        } else if (attribute.equalsIgnoreCase(Constants.PRODUCT_STOCK_UNIT)) {
                            stockItemDo.stockUnit = text;

                        } else if (attribute.equalsIgnoreCase(Constants.PRODUCT_WEIGHT_UNIT)) {
                            stockItemDo.weightUnit = text;

                        } else if (attribute.equalsIgnoreCase(Constants.PRODUCT_VOLUME)) {
                            stockItemDo.productVolume = Double.parseDouble(text);

                        } else if (attribute.equalsIgnoreCase(Constants.PRODUCT_TOTAL_STOCK)) {
                            stockItemDo.totalStockUnit = text;

                        } else if (attribute.equalsIgnoreCase(Constants.PRODUCT_TOTAL_STOCK_VOLUME)) {
                            stockItemDo.totalStockVolume = text;
                        } else if (attribute.equalsIgnoreCase(Constants.PRODUCT_QTY)) {
                            stockItemDo.quantity = Double.parseDouble(text);

                        } else if (attribute.equalsIgnoreCase(Constants.O_XPICKUP_DROP)) {
                            stockItemDo.pickUpOrDrop = Integer.parseInt(text);
                        }
                        else if (attribute.equalsIgnoreCase(Constants.EQUIP_CODE)) {
                            serialListDO.equipmentCode = text;

                        } else if (attribute.equalsIgnoreCase(Constants.EQUIP_DES)) {
                            serialListDO.equipmentDes = text;

                        } else if (attribute.equalsIgnoreCase(Constants.SERIAL)) {
                            serialListDO.serialDO = text;
                        }
                        text = "";
                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }
//
//                    if (endTag.equalsIgnoreCase("LIN")) {
//                        vanStockDetailsDO.stockItemList.add(stockItemDo);
//                    }

                    if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").
                            equalsIgnoreCase("GRP2")) {
                        vanStockDetailsDO.stockItemList.add(stockItemDo);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "")
                            .equalsIgnoreCase("GRP3")) {
                        vanStockDetailsDO.serialListDOS.add(serialListDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ProgressDialogTask.getInstance().showProgress(mContext, false, "");

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ProgressDialogTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, vanStockDetailsDO);
        }
    }
}