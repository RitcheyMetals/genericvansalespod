package com.tbs.generic.vansales.prints;

import android.bluetooth.BluetoothDevice;

import java.util.ArrayList;

public class PrinterConstants {
    public static final String PROGRESS_CANCEL_MSG      = "Printing cancelled\n";
    public static final String PROGRESS_COMPLETE_MSG    = "Printing completed\n";
    public static final String PROGRESS_ENDDOC_MSG      = "End of document\n";
    public static final String PROGRESS_FINISHED_MSG    = "Printer connection closed\n";
    public static final String PROGRESS_NONE_MSG        = "Unknown progress message\n";
    public static final String PROGRESS_STARTDOC_MSG    = "Start printing document\n";
//    public static final String PRINTER_MAC_ADDRESS      = "00:06:66:82:C2:A7";
    public static final String PRINTER_NAME             = "PB51";
//    public static String PRINTER_MAC_ADDRESS      = "00:10:40:B9:59:BE";

    public static String PRINTER_MAC_ADDRESS            = "";
    public static ArrayList<BluetoothDevice> bluetoothDevices = new ArrayList<>();

    public static final int PrintCylinderDeliveryNotes   = 1;
    public static final int PrintBulkDeliveryNotes       = 2;
    public static final int PrintCylinderIssue           = 3;
    public static final int PrintInvoiceReport           = 4;
    public static final int PrintPaymentReport           = 5;
    public static final int PrintActivityReport          = 6;
    public static final int PrintUserActivityReport      = 7;

    public static String base64LogoPng = "";
    public static String base64Footer = "";

}
