package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class ProductDetailMainDO implements Serializable {

    public String productID = "";
    public String productName = "";
    public String productDescription = "";

    public String productCategory = "";
    public String productStatus = "";


    public String stockUnit = "";
    public String weightUnit = "";
    public String productWeight = "";

    public String salesUnit = "";
    public String accountingCode = "";

    public String taxLevel = "";
    public String basePrice = "";
  //  public ArrayList<ProductDetailsDO> productDetailsDOS = new ArrayList<>();


}
