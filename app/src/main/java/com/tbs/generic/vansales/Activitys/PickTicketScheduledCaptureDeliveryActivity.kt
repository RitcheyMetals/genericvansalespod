package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.*
import com.darsh.multipleimageselect.activities.AlbumSelectActivity
import com.darsh.multipleimageselect.models.Image
import com.tbs.generic.vansales.Adapters.CustomerReturnsProductsAdapter
import com.tbs.generic.vansales.Adapters.FilesPreviewAdapter
import com.tbs.generic.vansales.Adapters.ScheduledProductsAdapter
import com.tbs.generic.vansales.Model.ActiveDeliveryDO
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO
import com.tbs.generic.vansales.Model.FileDetails
import com.tbs.generic.vansales.Model.PodDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.listeners.StringListner
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.LogUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.include_image_caputure.*
import kotlinx.android.synthetic.main.include_signature.*
import kotlinx.android.synthetic.main.include_signature.btnSignature
import kotlinx.android.synthetic.main.include_signature.imv_signature
import kotlinx.android.synthetic.main.pickticket_scheduled_capture_delivery.*
import kotlinx.android.synthetic.main.pod_screen_1.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.util.*


//
class PickTicketScheduledCaptureDeliveryActivity : BaseActivity() {

    lateinit var podDo: PodDo

    private var shipmentProductsList: ArrayList<ActiveDeliveryDO>? = ArrayList()
    private var cloneShipmentProductsList: ArrayList<ActiveDeliveryDO> = ArrayList()
    private var type: Int = 1
    lateinit var fileDetailsDo: ArrayList<FileDetails>

    private var fileDetailsList: java.util.ArrayList<FileDetails>? = null
    private lateinit var filesPreviewAdapter: FilesPreviewAdapter
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var btnRequestApproval: Button
    lateinit var btnConfirm: Button
    lateinit var tvNoDataFound: TextView
    lateinit var tvApprovalStatus: TextView
    private var shipmentType: String = ""
    var docType=0
    //    lateinit var cbNonBGSelected: CheckBox
    var status = ""
    var customerId = ""
    private var signature: String? = null

    lateinit var dialog: Dialog
    private var shipmentId: String = ""
    private var loadStockAdapter: ScheduledProductsAdapter? = null
    lateinit var activeDeliveryMainDO: ActiveDeliveryMainDO
    var nonBgType = 0

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.pickticket_scheduled_capture_delivery, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        hideKeyBoard(llBody)
        toolbar.setNavigationOnClickListener {

            if (isSentForApproval) {
                showToast(getString(R.string.please_process_order))
            } else {
                LogUtils.debug("PAUSE", "tes")
                var data = tvApprovalStatus.text.toString()
                var intent = Intent()
                intent.putExtra("Status", data)
                var args = Bundle()
                args.putSerializable("List", shipmentProductsList)
                intent.putExtra("BUNDLE", args)
                setResult(56, intent)
                finish()
            }
        }

        initializeControls()
       fileDetailsDo = StorageManager.getInstance(this).getImagesData(this)

        activeDeliveryMainDO = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
        podDo = StorageManager.getInstance(this).getDepartureData(this);

        if (intent.hasExtra("CustomerId")) {
            customerId = intent.extras?.getString("CustomerId")!!
        }

        ivRefresh.visibility = View.GONE
        btnRequestApproval.visibility= GONE
        btnSignature.setOnClickListener {
            val intent = Intent(this, CommonSignatureActivity::class.java)
            startActivityForResult(intent, 9907)
        }
        if(podDo.signature!!.isNotEmpty()){
            val decodedString = android.util.Base64.decode(podDo.signature, android.util.Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            imv_signature.visibility = VISIBLE
            imv_signature?.setImageBitmap(decodedByte)
        }

        imagesSetUp()
        btnAddImages.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this, AlbumSelectActivity::class.java)
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 1)
            if (fileDetailsList!!.size > 0 && fileDetailsList!!.size <= 4) {
                intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 4 - fileDetailsList!!.size)
            } else {
                intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 4)
            }
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_FROM, true)
            startActivityForResult(intent, 888)
        }
        var podDo = StorageManager.getInstance(this).getDepartureData(this)
        if (podDo.deliveryStatus?.get(0)!!.contains("Requested", true)) {
            tvApprovalStatus.visibility = VISIBLE
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirm.isClickable = false
            btnConfirm.isEnabled = false
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
            tvApprovalStatus.text = podDo.deliveryStatus?.get(0)
        } else if (podDo.deliveryStatus?.get(0)!!.contains("Approved", true)) {
            tvApprovalStatus.visibility = VISIBLE

            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
            btnConfirm.isClickable = true
            btnConfirm.isEnabled = true
            tvApprovalStatus.text = podDo.deliveryStatus?.get(0)

        } else {
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
            btnRequestApproval.isClickable = true
            btnRequestApproval.isEnabled = true
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
            btnConfirm.isClickable = true
            btnConfirm.isEnabled = true
            tvApprovalStatus.text = podDo.deliveryStatus?.get(0)
        }

        docType = preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0);

        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
            btnConfirm.text = getString(R.string.create_delivery)
        } else {
            btnConfirm.text = getString(R.string.create_delivery)
        }
        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
            if (tvApprovalStatus.text.toString().contains("Requested")) {
                btnConfirm.text = getString(R.string.create_delivery)
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirm.isClickable = false
                btnConfirm.isEnabled = false
                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnRequestApproval.isClickable = false
                btnRequestApproval.isEnabled = false
            } else {
                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                    btnConfirm.text = getString(R.string.create_delivery)
                    btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnConfirm.isClickable = false
                    btnConfirm.isEnabled = false
//                    cbNonBGSelected.setVisibility(GONE)
                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnRequestApproval.isClickable = false
                    btnRequestApproval.isEnabled = false
                } else {
                    btnConfirm.text = getString(R.string.create_delivery)
                    btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                    btnConfirm.isClickable = true
                    btnConfirm.isEnabled = true
//                    cbNonBGSelected.setVisibility(VISIBLE)

                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnRequestApproval.isClickable = false
                    btnRequestApproval.isEnabled = false
                }

            }
            loadScheduleData()

        } else {
            loadStockAdapter = ScheduledProductsAdapter(this, ArrayList(), "")
            recycleview.adapter = loadStockAdapter
            btnConfirm.text = getString(R.string.create_delivery)
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirm.isClickable = false
            btnConfirm.isEnabled = false
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
            ivRefresh.visibility = View.GONE
            btnRequestApproval.visibility = View.GONE
        }
        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
            btnConfirm.text = getString(R.string.confirm)
            ivAdd.visibility = VISIBLE
        } else {
            btnConfirm.text = getString(R.string.create_delivery)
            ivAdd.visibility = VISIBLE

        }
        ivAdd.setOnClickListener {
            Util.preventTwoClick(it)
            if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                val intent = Intent(this, TrailerEquipmentsActivity::class.java)
                intent.putExtra("FROM", "SCHEDULED")
                startActivityForResult(intent, 1)
            } else {
                val intent = Intent(this, ScheduledAddProductActivity::class.java)
                intent.putExtra("FROM", "SPOTSALES")
                startActivityForResult(intent, 1)
            }

        }

        btnRequestApproval.setOnClickListener {
            Util.preventTwoClick(it)
            requestApproval()
        }
        llPTnoteFab.setOnClickListener {
            var intent = Intent(this@PickTicketScheduledCaptureDeliveryActivity, PODNotesActivity::class.java);
            startActivityForResult(intent, 140);
        }
        btnConfirm.setOnClickListener {
            if (signature.isNullOrEmpty()&&podDo.signature.isNullOrEmpty()) {
                showToast(resources.getString(R.string.please_do_sig))
            }else{
                Util.preventTwoClick(it)
                if (!lattitudeFused.isNullOrEmpty()) {
                    stopLocationUpdates()
                    hideLoader()
                    creation()

                } else {
                    showLoader()
                    stopLocationUpdates()
                    location(ResultListner { `object`, isSuccess ->
                        if (isSuccess) {
                            hideLoader()
                            stopLocationUpdates()
                            creation()
                        }
                    })
                }
            }

        }
    }
    private fun saveDepartureData() {
        StorageManager.getInstance(this).saveDepartureData(this, podDo);
    }
       fun creation(){
           podDo.creDocLattitude = lattitudeFused
           podDo.creDOCLongitude = longitudeFused
           saveDepartureData()
           shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
           if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
               customerAuthorization(activeDeliveryMainDO.customer, 1, ResultListner { `object`, isSuccess ->

                   if (isSuccess) {
//                       preferenceUtils.saveString(PreferenceUtils.PICK_TICKET, createDeliveryMAinDo.deliveryNumber)
                     var  delivery = preferenceUtils.getStringFromPreference(PreferenceUtils.PICK_TICKET, "")
                            validatedelivery(delivery)
//                       pickTicketCreateDelivery()
                   } else {
                       showAlert(resources.getString(R.string.not_authorized))

                   }
               })
           } else {
               var customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
               customerAuthorization(customerDo.customer, 1, ResultListner { `object`, isSuccess ->

                   if (isSuccess) {
                       createDelivery(customerDo.customer)
                   } else {
                       showAlert(resources.getString(R.string.not_authorized))

                   }
               })
           }
       }
    private fun createDelivery(id: String) {


        if (Util.isNetworkAvailable(this)) {
            var siteId = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")

            var createDeliveryRequest = CreateDeliveryRequest(type, siteId, id, shipmentProductsList, this)

            createDeliveryRequest.setOnResultListener { isError, deliveryMainDo, msg ->
                hideLoader()
                var createDeliveryMAinDo = deliveryMainDo
                if (isError) {

                    hideLoader()
                    if (msg.isNotEmpty()) {
                        showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.success), false)

                    }
                    preferenceUtils . saveString (PreferenceUtils.DELIVERY_NOTE, "SUCCESS")

                    isSentForApproval = false
                } else {
                    if (createDeliveryMAinDo.status == 20) {
                        if (createDeliveryMAinDo.deliveryNumber.length > 0) {
                            preferenceUtils.saveString(PreferenceUtils.SPOT_DELIVERY_NUMBER, createDeliveryMAinDo.deliveryNumber)
                            if (createDeliveryMAinDo.message.length > 0) {
                                updateTheProductsInLocal()
                                validatedelivery(createDeliveryMAinDo.deliveryNumber)
                            }


                        } else {
                            if (createDeliveryMAinDo.message.length > 0) {
                                showAppCompatAlert(getString(R.string.error), "" + createDeliveryMAinDo.message, getString(R.string.ok), "", getString(R.string.success), false)

                            } else {
                                showAppCompatAlert(getString(R.string.error), getString(R.string.delivery_not_created), getString(R.string.ok), "", getString(R.string.success), false)

                            }
                            isSentForApproval = false
                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")

                        }
                    } else {
                        hideLoader()
                        if (createDeliveryMAinDo.message.length > 0) {
                            showAppCompatAlert(getString(R.string.error), "" + createDeliveryMAinDo.message, "Ok", "", "SUCCESS", false)

                        } else {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.delivery_not_created), getString(R.string.ok), "", getString(R.string.success), false)

                        }
                        isSentForApproval = false
                        preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")

                    }
                }
            }
            createDeliveryRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }

    }


    private fun pickTicketCreateDelivery() {


        if (Util.isNetworkAvailable(this)) {
            var pickTicket = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

            var createDeliveryRequest = PickTicketCreateDeliveryRequest(pickTicket, shipmentProductsList, this)

            createDeliveryRequest.setOnResultListener { isError, deliveryMainDo,msg ->
                hideLoader()
                var createDeliveryMAinDo = deliveryMainDo
                if (isError) {

                    hideLoader()
                    preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")

                    isSentForApproval = false
                    if(msg.isNotEmpty()){
                        showAppCompatAlert(getString(R.string.error),msg, getString(R.string.ok), "", getString(R.string.failure), false)

                    }else{
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)

                    }
                } else {
                    if (createDeliveryMAinDo.status == 2) {
                        if (createDeliveryMAinDo.deliveryNumber.length > 0) {


                            preferenceUtils.saveString(PreferenceUtils.PICK_TICKET, createDeliveryMAinDo.deliveryNumber)
                            preferenceUtils.saveInt(PreferenceUtils.DOC_TYPE_PICK, 4)



                            if (createDeliveryMAinDo.message.length > 0) {
                                showToast("" + createDeliveryMAinDo.message)
                                updateTheProductsInLocal()
                                validatedelivery(createDeliveryMAinDo.deliveryNumber)


                            }
                        } else {
                            if (createDeliveryMAinDo.message.length > 0) {
                                showAppCompatAlert(getString(R.string.error), "" + createDeliveryMAinDo.message, getString(R.string.ok), "", getString(R.string.failure), false)

                            } else {
                                showAppCompatAlert(getString(R.string.error), getString(R.string.delivery_not_created), getString(R.string.ok), "", getString(R.string.failure), false)

                            }
                            isSentForApproval = false
                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")

                        }

//
//                          val driverAdapter = CreateDeliveryProductAdapter(this, deliveryMainDo.nonSheduledProductDOS)
//                          recycleview.setAdapter(driverAdapter)
                    } else {
                        hideLoader()
                        if (createDeliveryMAinDo.message.length > 0) {
                            showAppCompatAlert(getString(R.string.error), "" + createDeliveryMAinDo.message, getString(R.string.ok), "", getString(R.string.failure), false)

                        } else {
                            if(msg.isNotEmpty()){
                                showAppCompatAlert(getString(R.string.error),msg, getString(R.string.ok), "", getString(R.string.success), false)

                            }else{
                                showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)

                            }
                        }
                        isSentForApproval = false
                        preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")

                    }
                }
            }
            createDeliveryRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }

    }

    private fun updateQtyConfirmationToServer() {
        if (Util.isNetworkAvailable(this)) {

            val request = ConfirmationQtyRequest(shipmentId, shipmentProductsList, this@PickTicketScheduledCaptureDeliveryActivity)
            request.setOnResultListener { isError, approveDO ->
                hideLoader()

                if (approveDO != null) {
                    if (isError) {
                        isSentForApproval = false

                        showAppCompatAlert("", "Please wait for confirmation approval", "Ok", "Cancel", "", false)

                    } else {
                        if (approveDO.flag == 2) {
                            updateTheProductsInLocal()
                            showToast(resources.getString(R.string.upload_successfully))
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                val intent = Intent(this@PickTicketScheduledCaptureDeliveryActivity, SignatureActivity::class.java)
                                startActivityForResult(intent, 11)
                            } else {
                                val intent = Intent(this@PickTicketScheduledCaptureDeliveryActivity, SignatureActivity::class.java)
                                startActivityForResult(intent, 11)

                            }

                        } else if (approveDO.flag == 1) {
                            updateTheProductsInLocal()
                            showToast("Shipment Already Validated")
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                val intent = Intent(this@PickTicketScheduledCaptureDeliveryActivity, SignatureActivity::class.java)
                                startActivityForResult(intent, 11)
                            } else {
                                val intent = Intent(this@PickTicketScheduledCaptureDeliveryActivity, SignatureActivity::class.java)
                                startActivityForResult(intent, 11)
                            }

                        } else {
                            showAppCompatAlert("", "Please wait for confirmation approval", "Ok", "Cancel", "", false)
                            isSentForApproval = false

                        }
                    }
                } else {
                    showAppCompatAlert("", "Please wait for confirmation approval", "Ok", "Cancel", "", false)
                    isSentForApproval = false

                }
            }
            request.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }

    }

    private fun updateMeterQtyToServer() {
        if (Util.isNetworkAvailable(this)) {

            val siteListRequest = ProductsModifyRequest(0, shipmentId, shipmentProductsList, this@PickTicketScheduledCaptureDeliveryActivity)
            siteListRequest.setOnResultListener { isError, approveDO ,msg->
                hideLoader()
                if (isError) {
                    Log.e("RequestAproval", "Response : " + isError)
                    isSentForApproval = false
                    updateQtyConfirmationToServer()

                } else {
                    if (approveDO != null) {
                        if (approveDO.flag == 4) {
                            updateQtyConfirmationToServer()

                        } else {
                            isSentForApproval = false
                            updateQtyConfirmationToServer()

                        }
                    } else {
                        updateQtyConfirmationToServer()

                    }


                }

            }
            siteListRequest.execute()

//            val request = UpdateRunQtyRequest(shipmentId, shipmentProductsList, this@ScheduledCaptureDeliveryActivity)
//            request.setOnResultListener { isError, approveDO ->
//                hideLoader()
//                //  updateTheProductsInLocal()
//                if (approveDO != null) {
//                    if (isError) {
//
//
//                        showAppCompatAlert("", " please try again.", "Ok", "Cancel", "", false)
//
//                    } else {
//                        if (approveDO != null) {
//                            if (approveDO.flag != null) {
//                                updateQtyConfirmationToServer()
//
////                            updateTheProductsInLocal()
//
//                                showToast("Updated Successfully")
//                            }
//                        } else {
//                            isSentForApproval = false
//
//                        }
//                    }
//                }
//            }
//            request.execute()
//


        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }

    }

    private fun updateTheProductsInLocal() {
        try {
            if (loadStockAdapter != null && loadStockAdapter!!.activeDeliveryDOS != null && loadStockAdapter!!.activeDeliveryDOS.size > 0) {

                val deliveryDos = loadStockAdapter!!.activeDeliveryDOS
                StorageManager.getInstance(this@PickTicketScheduledCaptureDeliveryActivity).saveCurrentDeliveryItems(this, deliveryDos)
                val updatedRows = StorageManager.getInstance(this).updateTheProductsInLocal(loadStockAdapter!!.activeDeliveryDOS)
//                if (updatedRows > 0) {
//                val intent = Intent()
//                intent.putExtra("DeliveredProducts", loadStockAdapter!!.activeDeliveryDOS)
//                intent.putExtra("Status", tvApprovalStatus.text.toString().trim())
//                setResult(1, intent)
//                finish()
//                } else {
//                    showToast("not confirmed")
//                }
            } else {
                showToast("There are no products to confirm")
            }
        } catch (e: Exception) {
        }
    }

    private fun loadScheduleData() {

        if (Util.isNetworkAvailable(this)) {
            if (shipmentId.length > 0) {
                val driverListRequest = ActiveDeliveryRequest(docType,shipmentId, this)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {
                        Log.e("loadScheduleData", "Error at loading loadScheduleData()")
                    } else {
                        StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)
                        shipmentProductsList = activeDeliveryDo.activeDeliveryDOS
                        if (shipmentProductsList != null && shipmentProductsList!!.size > 0) {
                            cloneShipmentProductsList.addAll(shipmentProductsList!!)
                            loadStockAdapter = ScheduledProductsAdapter(this, shipmentProductsList, "")
                            recycleview.adapter = loadStockAdapter
                            recycleview.visibility = VISIBLE
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
//                                cbNonBGSelected.visibility = GONE

                            } else {
//                                cbNonBGSelected.visibility = VISIBLE

                            }

                            tvNoDataFound.visibility = GONE
                            tvApprovalStatus.visibility = View.VISIBLE
                            btnRequestApproval.visibility = View.GONE
                            btnConfirm.visibility = View.VISIBLE

                        } else {
                            tvApprovalStatus.visibility = View.GONE
                            btnRequestApproval.visibility = View.GONE
                            btnConfirm.visibility = View.GONE
                            recycleview.visibility = GONE
                            tvNoDataFound.visibility = VISIBLE
                            btnConfirm.isEnabled = false
                            btnConfirm.isClickable = false
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        }
                    }
                }
                driverListRequest.execute()


            }
        }


//        }
    }

    fun deleteShipmentProducts(loadStockDO: ActiveDeliveryDO) {
        this.loadStockDO = loadStockDO
        showAppCompatAlert("", "Do you want to delete this product from this shipment?", "Delete", "Cancel", "DeleteProduct", false)
    }

    lateinit var loadStockDO: ActiveDeliveryDO
    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)
        if (from.equals("DeleteProduct", true)) {
            if (shipmentProductsList != null) {
                shipmentProductsList!!.remove(loadStockDO)
                updateConfirmRequestButtons(shipmentProductsList!!)
                loadStockAdapter = ScheduledProductsAdapter(this, shipmentProductsList, "")
                recycleview.adapter = loadStockAdapter
//                loadStockAdapter.refreshAdapter(shipmentProductsList)
                showSnackbar("Deleted successfully")
            }
        }
    }


    fun updateConfirmRequestButtons(shipmentProductsList: ArrayList<ActiveDeliveryDO>) {
//        if (!isSentForApproval) {
        shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")

        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
            if (isProductsChanged(shipmentProductsList)) {
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirm.isClickable = false
                btnConfirm.isEnabled = false
                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                btnRequestApproval.isClickable = true
                btnRequestApproval.isEnabled = true
                btnRequestApproval.visibility = GONE
                btnConfirm.visibility = VISIBLE
                tvApprovalStatus.text = ""
            } else {
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                btnConfirm.isClickable = true
                btnConfirm.isEnabled = true
                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnRequestApproval.isClickable = false
                btnRequestApproval.isEnabled = false
                btnRequestApproval.visibility = GONE
                btnConfirm.visibility = VISIBLE
            }
        } else {

        }

//        }
    }

    fun enableDisableConfirm(enable: Boolean) {
        // incase the product type meter reading using this method
        if (enable) {
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
        } else {
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
        }

        btnConfirm.isClickable = enable
        btnConfirm.isEnabled = enable
        btnRequestApproval.visibility = GONE
    }

    override fun initializeControls() {
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        btnRequestApproval = findViewById<View>(R.id.btnRequestApproval) as Button
        btnConfirm = findViewById<View>(R.id.btnConfirm) as Button
        tvNoDataFound = findViewById<View>(R.id.tvNoDataFound) as TextView
        tvApprovalStatus = findViewById<TextView>(R.id.tvApprovalStatus)
//        cbNonBGSelected = findViewById<View>(R.id.cbNonBGSelected) as CheckBox
//        cbNonBGSelected?.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) {
//                nonBgType = 1
//            } else {
//                nonBgType = 0
//            }
//        }

        tvScreenTitle.text = getString(R.string.confirm_products)

        var approval = tvApprovalStatus.text.toString()
        status = preferenceUtils.getStringFromPreference(PreferenceUtils.PRODUCT_APPROVAL, "")
        if (approval.equals("NA")) {
            tvApprovalStatus.text = getString(R.string.na)

        } else {
            tvApprovalStatus.text = status

        }

        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        ivRefresh.setOnClickListener {
            Util.preventTwoClick(it)
            if (tvApprovalStatus.text.toString().contains("Request")) {
                preferenceUtils.saveString(PreferenceUtils.PRODUCT_APPROVAL, tvApprovalStatus.text.toString())
                updateStatus()
            }
        }
    }
    private fun imagesSetUp() {
        fileDetailsList = fileDetailsDo
        filesPreviewAdapter = FilesPreviewAdapter(this, fileDetailsList, StringListner {

            if (fileDetailsList!!.size < 4) {
                btnAddImages.visibility = VISIBLE

            } else {
                btnAddImages.visibility = GONE
            }

            if (fileDetailsList!!.size >= 1) {
                recycleviewImages.visibility = VISIBLE
            } else {
                recycleviewImages.visibility = GONE
            }
        })
        recycleviewImages.adapter = filesPreviewAdapter
        if(fileDetailsList!!.size>0){
            recycleviewImages.visibility = VISIBLE

        }
    }

    private fun requestApproval() {
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = ProductsModifyRequest(4, shipmentId, shipmentProductsList, this@PickTicketScheduledCaptureDeliveryActivity)
            siteListRequest.setOnResultListener { isError, approveDO,msg ->
                hideLoader()
                if (approveDO != null) {
                    if (isError) {
                        Log.e("RequestAproval", "Response : " + isError)
                    } else {
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                        if (approveDO.flag == 4) {
                            tvApprovalStatus.text = getString(R.string.statue_requested)
                            isSentForApproval = true
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false
                        } else {
                            Toast.makeText(this@PickTicketScheduledCaptureDeliveryActivity, "Please send request again", Toast.LENGTH_SHORT).show()
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnRequestApproval.isClickable = true
                            btnRequestApproval.isEnabled = true
                            tvApprovalStatus.text = getString(R.string.statues_request_not_sent)
                        }

                    }
                }
            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }

    private var isSentForApproval: Boolean = false
    private fun updateStatus() {
        val siteListRequest = CancelRescheduleApprovalStatusRequest(shipmentId, this)
        siteListRequest.setOnResultListener { isError, approvalDO ->
            hideLoader()
            if (approvalDO != null) {
                if (isError) {
                    Toast.makeText(this@PickTicketScheduledCaptureDeliveryActivity, "Approval under progress", Toast.LENGTH_SHORT).show()
                } else {
//                    approvalDO.status = "Approved"
                    if (approvalDO.status.equals("Approved", true)) {
                        loadDeliveryData()
                        isSentForApproval = true
                        tvApprovalStatus.text = getString(R.string.status) + " : " + approvalDO.status
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false


                    } else if (approvalDO.status.equals("Rejected", true)) {
                        rejectProducts()
                        isSentForApproval = true
                        tvApprovalStatus.text = getString(R.string.status) + " : " + approvalDO.status
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false

                    } else {
                        Toast.makeText(this@PickTicketScheduledCaptureDeliveryActivity, "Approval under progress", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        siteListRequest.execute()
    }


    private fun isProductsChanged(shipmentProductsList: ArrayList<ActiveDeliveryDO>): Boolean {
        if (cloneShipmentProductsList != null && shipmentProductsList != null) {
            if (shipmentProductsList.size != cloneShipmentProductsList.size) {
                return true
            } else if (shipmentProductsList.size == cloneShipmentProductsList.size) {
                for (i in cloneShipmentProductsList.indices) {
                    if (shipmentProductsList.get(i).orderedQuantity != cloneShipmentProductsList.get(i).totalQuantity) {
                        return true
                    }
                }
            } else {
                return false
            }
        }
        return false
    }
    private fun setUpImagesAs64Bit() {
        podDo!!.capturedImagesListBulk=ArrayList()
        for (list in this.fileDetailsList!!) {
            try {
                val fis = FileInputStream(File(list.filePath))
                val bitmap = BitmapFactory.decodeStream(fis)
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val bytes = baos.toByteArray()
                val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                LogUtils.debug("PIC-->", encImage)
                podDo!!.capturedImagesListBulk!!.add(encImage)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        StorageManager.getInstance(this).saveDepartureData(this@PickTicketScheduledCaptureDeliveryActivity, podDo)

    }

    private fun validatedelivery(document:String) {
        setUpImagesAs64Bit()
        val driverListRequest = ValidateDeliveryRequest(podDo!!.capturedImagesListBulk,signature,podDo!!.deliveryNotes, document, lattitudeFused, longitudeFused, addressFused, this@PickTicketScheduledCaptureDeliveryActivity)
        driverListRequest.setOnResultListener { isError, validateDo, msg ->
            hideLoader()
            if (isError) {
                hideLoader()
                if (msg.isNotEmpty()) {
                    showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.failure), false)
                } else {
                    showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)
                }
            } else {

                if (validateDo.status == 20) {
                    preferenceUtils.saveInt(PreferenceUtils.VALIDATED_FLAG, 2)
                    hideLoader()

                    if (validateDo.message.length > 0) {
                        showToast("" + validateDo.message)

                    } else {

                        Toast.makeText(this@PickTicketScheduledCaptureDeliveryActivity, getString(R.string.shipment_validated), Toast.LENGTH_SHORT).show()
                    }
                    podDo!!.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                    podDo!!.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                    val intent = Intent()
                    intent.putExtra("CRELAT", lattitudeFused)
                    intent.putExtra("CRELON", longitudeFused)
                    intent.putExtra("RATING", podDo!!.departureRating)
                    intent.putExtra("NOTES", podDo!!.notes)
                    intent.putExtra("NAME", podDo!!.name)
                    setResult(1, intent)
                    finish()
                } else {
                    if (validateDo.message.length > 0) {
                        showToast("" + validateDo.message)
                    } else {
                        showToast(getString(R.string.shipment_not_validate))

                    }
                    preferenceUtils.saveInt(PreferenceUtils.VALIDATED_FLAG, 1)

                }

            }
        }
        driverListRequest.execute()


    }
    override fun onBackPressed() {

        if (isSentForApproval) {
            showToast("Please process the order..!")
        } else {
            LogUtils.debug("PAUSE", "tes")
            var data = tvApprovalStatus.text.toString()
            var intent = Intent()
            intent.putExtra("Status", data)
            intent.putExtra("List", shipmentProductsList)
            setResult(56, intent)
            finish()
            super.onBackPressed()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
            var from = data!!.getStringExtra("FROM")

            val activeDeliveryDOS = data.getSerializableExtra("AddedProducts") as ArrayList<ActiveDeliveryDO>
            if (activeDeliveryDOS.size > 0) {
                recycleview.visibility = VISIBLE
                tvNoDataFound.visibility = GONE

                if (loadStockAdapter == null) {
                    loadStockAdapter = ScheduledProductsAdapter(this, activeDeliveryDOS, resources.getString(R.string.pickup))
                    recycleview.adapter = loadStockAdapter
                } else {
                    var isProductExisted = false
                    for (i in activeDeliveryDOS.indices) {
                        for (k in shipmentProductsList!!.indices) {
                            if (!shipmentProductsList!!.get(k).pType.equals("SCHEDULED") && activeDeliveryDOS.get(i).product.equals(shipmentProductsList!!.get(k).product, true)
                                    && activeDeliveryDOS.get(i).shipmentId.equals(shipmentProductsList!!.get(k).shipmentId, true)) {
                                shipmentProductsList!!.get(k).orderedQuantity = activeDeliveryDOS.get(i).orderedQuantity
                                isProductExisted = true
                                break
                            }
                        }
                        if (isProductExisted) {
                            isProductExisted = false
                            continue
                        } else {
                            shipmentProductsList!!.add(activeDeliveryDOS.get(i))
                        }
                    }
                    loadStockAdapter!!.refreshAdapter(shipmentProductsList)
                }
                if (!shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                    btnConfirm.text = getString(R.string.create_delivery)
                    val shipmentProductsType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "")

                    if (shipmentProductsType.equals(AppConstants.MeterReadingProduct, true)) {
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnConfirm.isClickable = false
                        btnConfirm.isEnabled = false
                        btnConfirm.visibility = VISIBLE
                        btnRequestApproval.visibility = GONE
                        ivRefresh.visibility = GONE
//                        cbNonBGSelected.visibility = GONE
                    } else {
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnConfirm.visibility = VISIBLE
                        btnRequestApproval.visibility = GONE
                        ivRefresh.visibility = GONE
//                        cbNonBGSelected.visibility = VISIBLE

                    }

                } else {
//                    updateConfirmRequestButtons(shipmentProductsList!!)
                }
            }


        }

        if (resultCode == RESULT_OK && requestCode == 888) {
            val images = data?.getParcelableArrayListExtra<Image>(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_IMAGES)

            if (images != null) {
               var i = 0
                val l = images.size
                while (i < l) {
                    val file1 = File(images[i].path)
                    val fileDetails = FileDetails()
                    fileDetails.fileName = file1.name
                    fileDetails.filePath = file1.absolutePath
                    fileDetailsList?.add(0, fileDetails)
                    StorageManager.getInstance(this).saveImagesData(this,fileDetailsList)

                    i++
                }
                filesPreviewAdapter.notifyDataSetChanged()

                if (fileDetailsList!!.size < 4) {
                    btnAddImages.visibility = View.VISIBLE
                } else {
                    btnAddImages.visibility = View.GONE
                }

                if (fileDetailsList!!.size >= 1) {
                    recycleviewImages.visibility = View.VISIBLE
                } else {
                    recycleviewImages.visibility = View.GONE
                }
            }
        }
        if (resultCode == RESULT_OK && requestCode == 9907) {
            signature = data!!.getStringExtra("Signature")
            Log.d("Signature----->", signature)
            var rating = data!!.getStringExtra("Rating")
            podDo!!.signature = signature

            var name = data!!.getStringExtra("Name")

            podDo!!.name = name
            podDo.departureRating = rating
            saveDepartureData()
            val decodedString = android.util.Base64.decode(signature, android.util.Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            imv_signature.visibility = VISIBLE
            imv_signature?.setImageBitmap(decodedByte)

        }
        if (requestCode == 140 && resultCode == Activity.RESULT_OK) {
            val returnString = data!!.getStringExtra(resources.getString(R.string.notes_remarks))
            LogUtils.debug(resources.getString(R.string.notes_remarks), returnString)
            podDo.notes = returnString;
            saveDepartureData()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        hideLoader()
    }

    private fun rejectProducts() {
        val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

        loadStockAdapter = ScheduledProductsAdapter(this, activeDeliverySavedDo.activeDeliveryDOS, "")
        recycleview.adapter = loadStockAdapter
    }

    private fun loadDeliveryData() {

        if (Util.isNetworkAvailable(this)) {
            if (shipmentId.length > 0) {
                cloneShipmentProductsList.removeAll(shipmentProductsList!!)
                val driverListRequest = ActiveDeliveryRequest(docType,shipmentId, this)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {
                        Log.e("loadScheduleData", "Error at loading loadScheduleData()")
                    } else {
                        StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)
                        shipmentProductsList = activeDeliveryDo.activeDeliveryDOS
                        if (shipmentProductsList != null && shipmentProductsList!!.size > 0) {
                            cloneShipmentProductsList.addAll(shipmentProductsList!!)
                            loadStockAdapter = ScheduledProductsAdapter(this, shipmentProductsList, "")
                            recycleview.adapter = loadStockAdapter
                            recycleview.visibility = VISIBLE
//                            cbNonBGSelected.visibility = VISIBLE
                            tvNoDataFound.visibility = GONE
                            tvApprovalStatus.visibility = View.VISIBLE
                            btnRequestApproval.visibility = View.GONE
                            btnConfirm.visibility = View.VISIBLE
                        } else {
                            tvApprovalStatus.visibility = View.GONE
                            btnRequestApproval.visibility = View.GONE
                            btnConfirm.visibility = View.GONE
                            recycleview.visibility = GONE
                            tvNoDataFound.visibility = VISIBLE
                            btnConfirm.isEnabled = false
                            btnConfirm.isClickable = false
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        }
                    }
                }
                driverListRequest.execute()


            }
        }


//        }
    }


}