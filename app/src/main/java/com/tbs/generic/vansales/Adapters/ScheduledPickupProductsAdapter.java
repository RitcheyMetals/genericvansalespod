package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.PurchaseRecieptInfoActivity;
import com.tbs.generic.vansales.Activitys.ScheduledCaptureDeliveryActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.ReasonMainDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.listeners.LoadStockListener;
import com.tbs.generic.vansales.listeners.ResultListner;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class ScheduledPickupProductsAdapter extends RecyclerView.Adapter<ScheduledPickupProductsAdapter.MyViewHolder> implements Filterable {
    public ArrayList<ActiveDeliveryDO> activeDeliveryDOS;
    private String imageURL;
    private double weight = 0;
    private double volume = 0;
    ValueFilter valueFilter;
    private Context context;
    private ReasonMainDO reasonMainDo;
    private LoadStockListener loadStockListener;
    double mass = 0.0;

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<ActiveDeliveryDO> filterList = new ArrayList<>();
                for (int i = 0; i < activeDeliveryDOS.size(); i++) {
                    if ((activeDeliveryDOS.get(i).product.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(activeDeliveryDOS.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = activeDeliveryDOS.size();
                results.values = activeDeliveryDOS;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            activeDeliveryDOS = (ArrayList<ActiveDeliveryDO>) results.values;
            notifyDataSetChanged();
        }

    }

    public void refreshAdapter(ArrayList<ActiveDeliveryDO> activeDeliveryDOS) {
        this.activeDeliveryDOS = activeDeliveryDOS;
        notifyDataSetChanged();
    }

    public ArrayList<ActiveDeliveryDO> selectedactiveDeliveryDOs = new ArrayList<>();
    private String from = "";

    public ArrayList<ActiveDeliveryDO> getSelectedactiveDeliveryDOs() {
        return selectedactiveDeliveryDOs;
    }

    public ScheduledPickupProductsAdapter(Context context, ArrayList<ActiveDeliveryDO> activeDeliveryDOS, String from) {
        this.context = context;
        this.activeDeliveryDOS = activeDeliveryDOS;
        this.from = from;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_stock_data, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ActiveDeliveryDO activeDeliveryDO = activeDeliveryDOS.get(position);
        holder.tvProductName.setText(activeDeliveryDO.product);
        holder.tvDescription.setText(activeDeliveryDO.productDescription);

        if(activeDeliveryDO.serialLotFlag==4){
            holder.btnSerialLot.setEnabled(false);
            holder.btnSerialLot.setClickable(false);
            holder.btnSerialLot.setBackgroundColor(context.getResources().getColor(R.color.md_gray_light));
            holder.tvSerial.setText(context.getString(R.string.serial_managed_no));
            holder.tvLot.setText(context.getString(R.string.lot_managed_no));

        }else {
            holder.btnSerialLot.setEnabled(true);
            holder.btnSerialLot.setClickable(true);
            holder. btnSerialLot.setBackgroundColor(context.getResources().getColor(R.color.md_green));

            if(activeDeliveryDO.serialLotFlag==1){
                holder.tvSerial.setText(context.getString(R.string.serial_managed_yes));
                holder.tvLot.setText(context.getString(R.string.lot_managed_no));

            }else if(activeDeliveryDO.serialLotFlag==2){
                holder.tvSerial.setText(context.getString(R.string.serial_managed_no));
                holder.tvLot.setText(context.getString(R.string.lot_managed_yes));

            }
            else if(activeDeliveryDO.serialLotFlag==3){
                holder.tvSerial.setText(context.getString(R.string.serial_managed_yes));
                holder.tvLot.setText(context.getString(R.string.lot_managed_yes));

            }else {
                holder.tvSerial.setText(context.getString(R.string.serial_managed_no));
                holder.tvLot.setText(context.getString(R.string.lot_managed_no));
                holder.btnSerialLot.setEnabled(false);
                holder.btnSerialLot.setClickable(false);
                holder.btnSerialLot.setBackgroundColor(context.getResources().getColor(R.color.md_gray_light));
            }

        }

        if (from.equalsIgnoreCase("AddProducts")) {
            activeDeliveryDO.orderedQuantity=1;
            holder.tvNumberET.setText("" + activeDeliveryDO.orderedQuantity);
        } else{
            holder.tvNumberET.setText("" + activeDeliveryDO.orderedQuantity);

        }


        holder.tvAvailableQty.setText("Stock : " + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);
        int pos = position +1;
        int num=pos*1000;
        activeDeliveryDO.line = num;
        int reasonId = ((BaseActivity) context).preferenceUtils.getIntFromPreference(PreferenceUtils.DAMAGE_REASON_ID, 0);
        if (reasonId > 0) {
            String reason = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DAMAGE_REASON, "");

            holder.tvSelection.setText("" + reason);
        }
        holder.tvSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.llSelectReason.performClick();
            }
        });

        String shipmentProductsType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "");
        activeDeliveryDO.shipmentProductType = shipmentProductsType;
        String shipmentType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "");
        if (from.equalsIgnoreCase("AddProducts")) {
            holder.cbSelected.setVisibility(View.VISIBLE);
        } else if (from.equalsIgnoreCase("Shipments")) {
            holder.tvNumberET.setVisibility(View.GONE);
            holder.ivRemove.setVisibility(View.GONE);
            holder.ivAdd.setVisibility(View.GONE);
            holder.tvAvailableQty.setVisibility(View.GONE);
            holder.tvNumber.setVisibility(View.VISIBLE);
            holder.cbSelected.setVisibility(View.GONE);
            holder.tvNumber.setText("Qty : " + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);

        }
//        else {
//            if(shipmentType.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
//                holder.cbSelected.setVisibility(View.GONE);
//            }
//            else {
//                holder.cbSelected.setVisibility(View.VISIBLE);
//            }
//        }

        if (activeDeliveryDO.shipmentProductType.equalsIgnoreCase(AppConstants.FixedQuantityProduct)
                && !from.equalsIgnoreCase("Shipments")) {
            holder.tvNumberET.setVisibility(View.VISIBLE);
            holder.ivRemove.setVisibility(View.VISIBLE);
            holder.ivAdd.setVisibility(View.VISIBLE);
            holder.tvNumber.setVisibility(View.GONE);
            holder.llMeterReadings.setVisibility(View.GONE);
            holder.llKGMeterReadings.setVisibility(View.GONE);

            holder.llAddRemove.setVisibility(View.VISIBLE);
            holder.tvAvailableQty.setText(context.getString(R.string.stock)+" : " + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);
            if (from.equalsIgnoreCase("AddProducts")) {
                holder.tvAvailableQty.setVisibility(View.VISIBLE);
            }else {
                holder.tvAvailableQty.setVisibility(View.GONE);

            }
//            activeDeliveryDO.orderedQuantity = activeDeliveryDO.totalQuantity;
//            holder.tvNumberET.setText("" + activeDeliveryDO.totalQuantity);// to fix issue at added products showing full qty instead selected qty in add productlistactivity
            holder.tvNumberET.setText("" + activeDeliveryDO.orderedQuantity);
        } else if (activeDeliveryDO.shipmentProductType.equalsIgnoreCase(AppConstants.MeterReadingProduct)
                && !from.equalsIgnoreCase("Shipments") && !from.equalsIgnoreCase("AddProducts")) {
            holder.tvNumberET.setVisibility(View.VISIBLE);
            holder.ivRemove.setVisibility(View.VISIBLE);
            holder.ivAdd.setVisibility(View.VISIBLE);
            holder.tvNumber.setVisibility(View.GONE);
            holder.etTotalMeterQty.setText("" + activeDeliveryDO.orderedQuantity);
            holder.etKGTotalMeterQty.setText("" + activeDeliveryDO.orderedQuantity);

            holder.tvNumberET.setText("");
            if (activeDeliveryDO.productUnit.length() > 0) {
                holder.tvProductUnit.setText("Qty1" + (activeDeliveryDO.productUnit));
                if (activeDeliveryDO.productUnit.equalsIgnoreCase("KG")) {
                    holder.llKGMeterReadings.setVisibility(View.VISIBLE);
                    holder.llMeterReadings.setVisibility(View.GONE);
                    holder.tvKGProductUnit.setText("Qty1 (" + (activeDeliveryDO.productUnit) + ")");

                } else {
                    holder.llMeterReadings.setVisibility(View.VISIBLE);
                    holder.llKGMeterReadings.setVisibility(View.GONE);
                    holder.tvProductUnit.setText("Qty1 (" + (activeDeliveryDO.productUnit) + ")");

                }

            } else {
                holder.llMeterReadings.setVisibility(View.VISIBLE);
                holder.llKGMeterReadings.setVisibility(View.GONE);

            }
            holder.llAddRemove.setVisibility(View.GONE);
//            holder.etTotalMeterQty.setText("");
//            holder.etTotalMeterQty.append("" + activeDeliveryDO.totalQuantity);
//            holder.etKGTotalMeterQty.setText("");
//            holder.etKGTotalMeterQty.append("" + activeDeliveryDO.totalQuantity);

            holder.tvAvailableQty.setVisibility(View.GONE);
        } else if (activeDeliveryDO.productType.equalsIgnoreCase(AppConstants.ShipmentListView)) {
            holder.tvNumberET.setVisibility(View.GONE);
            holder.ivRemove.setVisibility(View.GONE);
            holder.ivAdd.setVisibility(View.GONE);
            holder.tvNumber.setVisibility(View.VISIBLE);
            holder.tvAvailableQty.setVisibility(View.GONE);
            holder.tvNumber.setText("" + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);
        }

        holder.cbSelected.setOnCheckedChangeListener(null);
        holder.cbSelected.setChecked(activeDeliveryDO.isProductAdded);
        holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                activeDeliveryDO.isProductAdded = isChecked;
                if (isChecked) {
                    selectedactiveDeliveryDOs.add(activeDeliveryDO);
                } else {
                    selectedactiveDeliveryDOs.remove(activeDeliveryDO);
                }
            }
        });
        int maxCount = activeDeliveryDO.totalQuantity;//Integer.parseInt(holder.tvNumberET.getText().toString());
        final int[] quantity = {activeDeliveryDO.orderedQuantity};//{Integer.parseInt(holder.tvNumberET.getText().toString())};
        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quantity[0] < maxCount) {
                    quantity[0] = quantity[0] + 1;
                    holder.tvNumberET.setText("" + quantity[0]++);
                    activeDeliveryDO.orderedQuantity = quantity[0];
                    if (context instanceof ScheduledCaptureDeliveryActivity) {

                        ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons(activeDeliveryDOS);


                    }

                }
            }
        });
        holder.ivRemove.setOnClickListener(view -> {
            if (quantity[0] > 1) {
                quantity[0] = quantity[0] - 1;
                holder.tvNumberET.setText("" + quantity[0]);
                activeDeliveryDO.orderedQuantity = quantity[0];
                if (context instanceof ScheduledCaptureDeliveryActivity) {
                    ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons(activeDeliveryDOS);
                }

            }
        });
        holder.tvNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equalsIgnoreCase("")) {
                    if (Integer.parseInt(s.toString()) <= maxCount && Integer.parseInt(s.toString()) > 0) {
                        quantity[0] = Integer.parseInt(s.toString());
                        activeDeliveryDO.orderedQuantity = quantity[0];
                        if (context instanceof ScheduledCaptureDeliveryActivity) {
                            ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons(activeDeliveryDOS);
                        }
                    } else {
                        holder.tvNumberET.setText("1");
                        if (s.length() > 1) {
                            final String newText = s.toString().substring(0, s.length() - 1) + "";
                            holder.tvNumberET.setText("" + newText);
                        }
                    }
                } else {
                    holder.tvNumberET.setText("1");
                }
            }
        });
        if (context instanceof ScheduledCaptureDeliveryActivity) {
            holder.ivDelete.setVisibility(View.GONE);
        } else {
            holder.ivDelete.setVisibility(View.GONE);
        }
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof ScheduledCaptureDeliveryActivity) {
                    ((ScheduledCaptureDeliveryActivity) context).deleteShipmentProducts(activeDeliveryDO);
                }
            }
        });
        holder.btnSerialLot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(from.equalsIgnoreCase("PICKUP")){
                    Intent intent= new Intent(context, PurchaseRecieptInfoActivity.class);
                    intent.putExtra("P_ID",activeDeliveryDO.product);
                    intent.putExtra("PRODUCT",activeDeliveryDO.productDescription);
                    intent.putExtra("PRODUCTDO", activeDeliveryDO);
                    ((BaseActivity) context).startActivityForResult(intent,10);
                    holder.btnSerialLot.setEnabled(false);
                    holder.btnSerialLot.setClickable(false);
                    holder.btnSerialLot.setBackgroundColor(context.getResources().getColor(R.color.md_gray_light));
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return activeDeliveryDOS != null ? activeDeliveryDOS.size() : 0;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName,tvSerial,tvLot, tvDescription, tvAvailableQty, tvInfo, tvNumber, etTotalMeterQty, etKGTotalMeterQty, tvSelection, tvProductUnit, tvKGProductUnit;
        public RelativeLayout rlRemove, rlAdd;
        public ImageView ivRemove, ivAdd, ivDelete;
        public LinearLayout llAddRemove, llMeterReadings, llSelectReason, llKGMeterReadings;
        public EditText tvNumberET, etMeterReading1, etMeterReading2, etMeterReading3, etKGMeterReading1, etKGMeterReading2, etKGMeterReading3;
        public Button btnSerialLot;

        private CheckBox cbSelected;

        public MyViewHolder(View view) {
            super(view);
            tvProductName     = view.findViewById(R.id.tvName);
            tvSerial          = view.findViewById(R.id.tvSerial);
            tvLot             = view.findViewById(R.id.tvLot);
            tvDescription     = view.findViewById(R.id.tvDescription);
            tvNumber          = view.findViewById(R.id.tvNumber);
            ivRemove          = view.findViewById(R.id.ivRemove);
            ivAdd             = view.findViewById(R.id.ivAdd);
            tvNumberET        = view.findViewById(R.id.tvNumberET);
            tvAvailableQty    = view.findViewById(R.id.tvAvailableQty);
            tvInfo            = view.findViewById(R.id.tvInfo);
            tvProductUnit     = view.findViewById(R.id.tvProductUnit);
            etMeterReading1   = view.findViewById(R.id.etMeterReading1);
            etMeterReading2   = view.findViewById(R.id.etMeterReading2);
            etTotalMeterQty   = view.findViewById(R.id.etTotalMeterQty);
            etMeterReading3   = view.findViewById(R.id.etMeterReading3);
            llMeterReadings   = view.findViewById(R.id.llMeterReadings);
            tvKGProductUnit   = view.findViewById(R.id.tvKGProductUnit);
            etKGMeterReading1 = view.findViewById(R.id.etKGMeterReading1);
            etKGMeterReading2 = view.findViewById(R.id.etKGMeterReading2);
            etKGTotalMeterQty = view.findViewById(R.id.etKGTotalMeterQty);
            etKGMeterReading3 = view.findViewById(R.id.etKGMeterReading3);
            llKGMeterReadings = view.findViewById(R.id.llKGMeterReadings);
            ivDelete          = view.findViewById(R.id.ivDelete);
            cbSelected        = view.findViewById(R.id.cbSelected);
            llAddRemove       = view.findViewById(R.id.llAddRemove);
            llSelectReason    = view.findViewById(R.id.llSelectReason);
            tvSelection       = view.findViewById(R.id.tvSelection);
            btnSerialLot       = view.findViewById(R.id.btnSerialLot);

        }
    }

}
