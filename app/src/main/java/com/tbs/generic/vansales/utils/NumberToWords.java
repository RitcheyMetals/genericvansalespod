package com.tbs.generic.vansales.utils;


import android.util.Log;

public class NumberToWords
{
    private static NumberToWords numberToWords;
    private static final String[] one = {" ", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve", " Thirteen", " Fourteen", "Fifteen", " Sixteen", " Seventeen", " Eighteen", " Nineteen"};
    private static final String[] ten = {" ", " ", " Twenty", " Thirty", " Forty", " Fifty", " Sixty", "Seventy", " Eighty", " Ninety"};

    public static NumberToWords getNumberToWords(){
        if (numberToWords == null){
            numberToWords = new NumberToWords();
        }
        return numberToWords;
    }

    private NumberToWords(){

    }

    private String numberToWords(int n,String ch) {
        if(n > 19) {
            return ten[n/10]+" "+one[n%10];
        }
        else {
            return one[n];
        }
//        if(n > 0){
//            System.out.print(ch);
//        }
  }
  public String getTextFromNumbers(double mNumber) {
        String numberToText  = "";
      try {
          int number = (int)mNumber;
          int decimal = ((int)(100 * mNumber) - 100 * number)/100;
          numberToText = getText(number)+" And "+getText(decimal);
      }
      catch (Exception e) {
          e.printStackTrace();
          return numberToText;
      }
      return numberToText;
  }

  private String getText(int number){
      if(number <= 0)   {
          Log.e("NumberToWords : ","Enter numbers greater than 0");
          return "";
      }
      else {
          return numberToWords((number/1000000000)," Hundred")+
                  numberToWords((number/10000000)%100," Crore")+
                  numberToWords(((number/100000)%100)," Lakh")+
                  numberToWords(((number/1000)%100)," Thousand")+
                  numberToWords(((number/100)%10)," Hundred")+
                  numberToWords((number%100)," ");
      }
  }
}