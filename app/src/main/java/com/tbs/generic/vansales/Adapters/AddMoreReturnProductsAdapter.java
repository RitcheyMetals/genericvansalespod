package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.LoanReturnDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class AddMoreReturnProductsAdapter extends RecyclerView.Adapter<AddMoreReturnProductsAdapter.ReturnListHolder> {

    private ArrayList<LoanReturnDO> loanReturnDos;
    private Context context;
    private PreferenceUtils preferenceUtils;
    private ArrayList<LoanReturnDO> selectedLoanReturnDOs = new ArrayList<>();

    public AddMoreReturnProductsAdapter(Context context, ArrayList<LoanReturnDO> loanReturnDos) {
        this.context = context;
        this.loanReturnDos = loanReturnDos;
        preferenceUtils = ((BaseActivity)context).preferenceUtils;
    }

    public ArrayList<LoanReturnDO> getSelectedNonBgDOs(){
        return selectedLoanReturnDOs;
    }

    @Override
    public ReturnListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.add_return_product_cell, parent, false);
        return new ReturnListHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ReturnListHolder holder, final int position) {
        final LoanReturnDO loanReturnDO = loanReturnDos.get(position);
        holder.tvProductName.setText(loanReturnDO.productName);
        holder.tvProductDescription.setText(loanReturnDO.productDescription);
        holder.etNumberET.setText(((int) loanReturnDO.qty)+ "");
        holder.tvLineNumber.setText("Stock Unit : "+loanReturnDO.unit);

        holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    if(loanReturnDO.qty == 0){
                        loanReturnDO.qty = 1;
                        holder.etNumberET.setText(""+((int)loanReturnDO.qty));
                    }
                    selectedLoanReturnDOs.add(loanReturnDO);
                }
                else {
                    selectedLoanReturnDOs.remove(loanReturnDO);
                }
            }
        });

//        int maxCount = (int) loanReturnDO.qty;//Integer.parseInt(holder.tvNumberET.getText().toString());
        final int[] quantity = {(int) loanReturnDO.qty};
        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
    //                if(quantity[0] < maxCount){
    //                }
                quantity[0]= quantity[0]+1;
                holder.etNumberET.setText(""+(quantity[0]++));
                loanReturnDO.qty= quantity[0];
            }
        });
        holder.ivRemove.setOnClickListener(view -> {
            if(quantity[0] > 1){
                quantity[0]= quantity[0]-1;
                holder.etNumberET.setText(""+quantity[0]);
                loanReturnDO.qty= quantity[0];
            }
        });
        holder.etNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equalsIgnoreCase("")){
                    quantity[0] = Integer.parseInt(s.toString());
                    loanReturnDO.qty = quantity[0];
//                    if(Integer.parseInt(s.toString())<=maxCount && Integer.parseInt(s.toString())>0){
//                    }
//                    else {
//                        final String newText = s.toString().substring(0, s.length()-1) + "";
//                        holder.etNumberET.setText(""+newText);
//                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return loanReturnDos!=null?loanReturnDos.size():0;
    }

    public class ReturnListHolder extends RecyclerView.ViewHolder{
        private TextView tvShipmentId, tvProductName, tvProductDescription, tvLineNumber;
        private CheckBox cbSelected;
        private EditText etNumberET;
        private ImageView ivAdd, ivRemove;

        private ReturnListHolder(@NonNull View itemView) {
            super(itemView);
            tvProductName           = itemView.findViewById(R.id.tvProductName);
            tvProductDescription    = itemView.findViewById(R.id.tvProductDescription);
            etNumberET              = itemView.findViewById(R.id.tvNumberET);
            cbSelected              =  itemView.findViewById(R.id.cbSelected);
            tvLineNumber            = itemView.findViewById(R.id.tvLineNumber);
            ivAdd                   = itemView.findViewById(R.id.ivAdd);
            ivRemove                = itemView.findViewById(R.id.ivRemove);

        }
    }
}
