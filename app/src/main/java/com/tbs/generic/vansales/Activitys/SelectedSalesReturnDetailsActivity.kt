package com.tbs.generic.vansales.Activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.tbs.generic.vansales.Adapters.LoanReturnAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.LoanReturnDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.reading_dialog.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

//
class SelectedSalesReturnDetailsActivity : BaseActivity() {
    lateinit var invoiceAdapter: LoanReturnAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound:TextView
    lateinit var tvBalanceQty:TextView
    lateinit var tvReturnedQty:TextView
    lateinit var tvIssuedQty:TextView
    lateinit var tvOpeningQty:TextView
    lateinit var openingQty:String
    private var issuedQty : Int = 0
    private var type : Int = 1

    private var   returnQty :Double=0.0
    private var balanceQty:Double=0.0
    private lateinit var btnConfirm: Button
    private lateinit var btnComment: Button

    private lateinit var rbEmptyCylinder: RadioButton
    private lateinit var rbSalesReturn: RadioButton
    private var loanReturnDOsMap: LinkedHashMap<String, ArrayList<LoanReturnDO>> = LinkedHashMap()
    private var loanReturnDos: ArrayList<LoanReturnDO> = ArrayList()

    override fun initialize() {
      val  llCategories = layoutInflater.inflate(R.layout.selected_salesreturn_details, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
//        ivAdd.setVisibility(View.VISIBLE)
        if(intent.hasExtra("IssuedQty"))
            issuedQty = intent.extras!!.getInt("IssuedQty", 0)

        if (intent.hasExtra("Products")) {
            loanReturnDos = intent.getSerializableExtra("Products") as ArrayList<LoanReturnDO>
        }
        if (intent.hasExtra("openingQty")) {
            openingQty = intent.extras!!.getString("openingQty", "")

        }

        initializeControls()
        val rgType= findViewById<RadioGroup>(R.id.rgType)

        rgType.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.rbEmptyCylinder -> {
                        type=1
                        refreshAdapter(type)
                    }
                    R.id.rbSalesReturn -> {
                        type=2
                        refreshAdapter(type)
                    }

                }
            }
        })
        tvOpeningQty.text = ""+openingQty
        tvIssuedQty.text = ""+issuedQty

        loanReturnDOsMap = LinkedHashMap<String, ArrayList<LoanReturnDO>>()

        bindData(loanReturnDos)


        btnConfirm.setOnClickListener {

                Util.preventTwoClick(it)
                if(loanReturnDOsMap!=null && loanReturnDOsMap.size>0){
                    StorageManager.getInstance(this).saveReturnCylinders(this, loanReturnDos)
                    val returnCount = StorageManager.getInstance(this).saveAllReturnStockValue(loanReturnDos)
                    if(returnCount>0){
                        var recievedSite = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")
                        var vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")

                        val customer = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER, "")
                        val returnDate = CalendarUtils.getDate()
                        if (Util.isNetworkAvailable(this)) {
                            val siteListRequest = CreateSalesReturnRequest( recievedSite,customer,vehicleCode,type,loanReturnDOsMap,this@SelectedSalesReturnDetailsActivity)
                            showLoader()
                            siteListRequest.setOnResultListener { isError, loanReturnMainDo ->
                                hideLoader()
                                if (loanReturnMainDo != null) {
                                    if (isError) {
                                        showToast(getString(R.string.unable_to_create_sales_returns))
                                    } else {
                                        if(loanReturnMainDo.status==20){
                                            val intent = Intent()
                                            intent.putExtra("CapturedReturns", true)
                                            setResult(11, intent)
                                            showToast(getString(R.string.sales_returns_created))

                                            finish()
                                        }else{
                                            showToast(getString(R.string.unable_to_create_sales_returns))
                                        }

                                    }
                                } else {
                                    showToast(getString(R.string.unable_to_create_sales_returns))
                                }

                            }
                            siteListRequest.execute()

                        } else {
                            showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

                        }



                    }
                    else{
                        showToast(getString(R.string.unbale_to_create_loan))
                    }
                }


        }


        setReturnQty(loanReturnDos)
    }

    private fun refreshAdapter(type : Int){
        if(type == 2){
            if(invoiceAdapter == null){
                invoiceAdapter = LoanReturnAdapter(this@SelectedSalesReturnDetailsActivity, loanReturnDOsMap,"")
                recycleview.adapter = invoiceAdapter
            }
            else{
                invoiceAdapter.refreshAdapter(loanReturnDOsMap, "")
            }
        }
        else {
            if(invoiceAdapter == null){
                invoiceAdapter = LoanReturnAdapter(this@SelectedSalesReturnDetailsActivity, loanReturnDOsMap,"salesreturn")
                recycleview.adapter = invoiceAdapter
            }
            else{
                invoiceAdapter.refreshAdapter(loanReturnDOsMap, "salesreturn")
            }
        }
    }

    private fun setReturnQty(loanReturnDos : ArrayList<LoanReturnDO>){
        if(loanReturnDos!=null && loanReturnDos.size>0){
            for (i in loanReturnDos.indices){
                returnQty = returnQty + loanReturnDos.get(i).qty
            }
        }
        tvReturnedQty.text = ""+returnQty
        openingQty.toDouble()
        tvBalanceQty.text = ""+(openingQty.toDouble()+issuedQty-returnQty)
        balanceQty = tvBalanceQty.text.toString().toDouble()
    }

    override fun initializeControls() {
        tvScreenTitle.text = getString(R.string.sales_return)
        recycleview    = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        tvNoDataFound  = findViewById<TextView>(R.id.tvNoDataFound)
        tvOpeningQty   = findViewById<TextView>(R.id.tvOpeningQty)
        tvIssuedQty    = findViewById<TextView>(R.id.tvIssuedQty)
        tvReturnedQty  = findViewById<TextView>(R.id.tvReturnedQty)
        tvBalanceQty   = findViewById<TextView>(R.id.tvBalanceQty)
        recycleview.layoutManager = linearLayoutManager
        btnConfirm     = findViewById<Button>(R.id.btnConfirm)
        rbEmptyCylinder  = findViewById<RadioButton>(R.id.rbEmptyCylinder)
        rbSalesReturn    = findViewById<RadioButton>(R.id.rbSalesReturn)
        btnComment     = findViewById<Button>(R.id.btnComment)
        btnComment.setOnClickListener {
            Util.preventTwoClick(it)
            showAddItemDialog()

        }


//        showLoader()
//        val driverListRequest = LoanReturnRequest(this@SelectedReturnDetailsActivity)
//        driverListRequest.setOnResultListener { isError, invoiceHistoryMainDOs ->
//            hideLoader()
//            if (isError) {
//                tvNoDataFound.setVisibility(View.VISIBLE)
//                recycleview.setVisibility(View.GONE)
//                Toast.makeText(this@SelectedReturnDetailsActivity, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
//            }
//            else {
//
//
//            }
//        }
//        driverListRequest.execute()

    }

    private fun bindData(loanReturnDos: ArrayList<LoanReturnDO>){

        val shipmentIds = ArrayList<String>()
        if(loanReturnDos!=null && loanReturnDos.size>0){
            for (i in loanReturnDos.indices){
                if(!shipmentIds.contains(loanReturnDos.get(i).shipmentNumber)){
                    shipmentIds.add(loanReturnDos.get(i).shipmentNumber)
                }
            }
            setReturnQty(loanReturnDos)
        }
        if(shipmentIds.size>0){
            for (j in shipmentIds.indices){
                val returnList = ArrayList<LoanReturnDO>()
                for (i in loanReturnDos.indices) {
                    if (loanReturnDos.get(i).shipmentNumber.equals(shipmentIds.get(j))) {
                        returnList.add(loanReturnDos.get(i))
                    }
                }
                loanReturnDOsMap.put(shipmentIds.get(j), returnList)
            }
        }
        if(loanReturnDOsMap.size>0){
            tvNoDataFound.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
            invoiceAdapter = LoanReturnAdapter(this@SelectedSalesReturnDetailsActivity, loanReturnDOsMap,"salesreturn")
            recycleview.adapter = invoiceAdapter
        }else{
            tvNoDataFound.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
            btnConfirm.visibility = View.GONE
        }

    }

    private fun showAddItemDialog() {
        try {
            var dialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(this).inflate(R.layout.comment_custom, null)
//            applyFont(view, AppConstants.MONTSERRAT_REGULAR_TYPE_FACE)
            var etAdd = view.findViewById<View>(R.id.etAdd) as EditText
            var comment =preferenceUtils.getStringFromPreference(PreferenceUtils.COMMENT_ID,"")
            if(comment.isNotEmpty()){
                etAdd.setText(comment)
                val pos: Int = etAdd.getText().length
                etAdd.setSelection(pos)
            }

            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            val btnSubmit = view.findViewById<View>(R.id.btnSubmit) as Button




            btnSubmit.setOnClickListener {
                Util.preventTwoClick(it)
                val itemName = etAdd.text.toString().trim()

                if (itemName.equals("", ignoreCase = true)) {
                    showToast(getString(R.string.please_enter_comment))
                } else {

                    preferenceUtils.saveString(PreferenceUtils.COMMENT_ID, itemName)
                    dialog.dismiss()
                }
            }
            dialog.setContentView(view)
            if (!dialog.isShowing)
                dialog.show()
        } catch (e: Exception) {
        }

    }




}