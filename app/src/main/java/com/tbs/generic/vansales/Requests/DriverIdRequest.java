package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.DriverDO;
import com.tbs.generic.vansales.Model.DriverEmptyDO;
import com.tbs.generic.vansales.Model.DriverIdMainDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class DriverIdRequest extends AsyncTask<String, Void, Boolean> {

    private DriverIdMainDO driverIdMainDO;
    private DriverDO driverDO;
    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    private DriverEmptyDO driverEmptyDO;
    private ArrayList<DriverEmptyDO> driverEmptyDOS;

    public DriverIdRequest(String id, Context mContext) {

        this.mContext = mContext;
        this.id = id;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, DriverIdMainDO driverIdMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_YDRIVER", id);
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.DRIVER_ID, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("Scheduled xmlString " + xmlString);
        try {
            String text = "", attribute = "", attribute1 = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            driverIdMainDO = new DriverIdMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        attribute1 = xpp.getAttributeValue(null, "ID");

                        if (attribute1.equalsIgnoreCase("GRP2")) {

                            driverIdMainDO.driverDOS = new ArrayList<>();
                        } else {

                            driverIdMainDO.driverEmptyDOS = new ArrayList<>();

                        }


                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        // String  attribute2 = xpp.getAttributeValue(null, "ID");
                        if (attribute1.equalsIgnoreCase("GRP3")) {

                            driverDO = new DriverDO();
                        } else {
                            driverEmptyDO = new DriverEmptyDO();
//
                        }
//                        if(attribute2.equalsIgnoreCase("GRP2")){
//                        }else {
                    }
//                    else if (startTag.equalsIgnoreCase("LIN")) {
//                            driverEmptyDO = new DriverEmptyDO();
//
//
//
//
//                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
//                        if (attribute.equalsIgnoreCase("I_YBPCNUM")) {
//                            customerDetailsDo.number(text);
//                            customerDetailsDos.add(customerDetailsDo);
//
//                        }else

                        if (attribute.      equalsIgnoreCase("I_YDRIVER")) {
                            driverIdMainDO.driverName = text;
                        } else if (attribute.equalsIgnoreCase("O_YFCYDES")) {
                            driverDO.site = text;
                        } else if (attribute.equalsIgnoreCase("O_YSLOC")) {
                            if (text.length() > 0) {
                                driverDO.location = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YLOCTYP")) {
                            if (text.length() > 0) {
                                driverIdMainDO.locationType = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YDNAME")) {
                            driverDO.driver = text;


                        } else if (attribute.equalsIgnoreCase("O_YTRIP")) {
                            driverDO.tripNumber = text;


                        } else if (attribute.equalsIgnoreCase("O_YVEHROU")) {
                            driverDO.vehicleRouteId = text;

                        } else if (attribute.equalsIgnoreCase("O_YCARRIER")) {
                            driverDO.vehicleCarrier = text;

                        } else if (attribute.equalsIgnoreCase("O_YVEHCODE")) {
                            driverDO.vehicleCode = text;

                        } else if (attribute.equalsIgnoreCase("O_YSITE")) {
                            driverDO.siteId = text;

                        } else if (attribute.equalsIgnoreCase("O_YVEHNO")) {
                            driverDO.plate = text;

                        } else if (attribute.equalsIgnoreCase("O_YOCOUNT")) {
                            if (text.length() > 0) {

                                driverDO.nShipments = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YARRDATE")) {
                            driverDO.aDate = text;

                        } else if (attribute.equalsIgnoreCase("O_YDLDATE")) {
                            driverDO.dDate = text;

                        } else if (attribute.equalsIgnoreCase("O_YDPTIME")) {
                            driverDO.dTime = text;

                        } else if (attribute.equalsIgnoreCase("O_YARRTIME")) {
                            driverDO.aTime = text;

                        } else if (attribute.equalsIgnoreCase("O_YEMPTVRU")) {
                            driverEmptyDO.emptyVehicleCode = text;

                        } else if (attribute.equalsIgnoreCase("O_YVEHNO1")) {
                            driverEmptyDO.plate = text;

                        } else if (attribute.equalsIgnoreCase("O_YOCOUNT1")) {
                            if (text.length() > 0) {

                                driverEmptyDO.nNonAssignedShipments = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YARRDATE1")) {
                            driverEmptyDO.aDate = text;

                        } else if (attribute.equalsIgnoreCase("O_YDLDATE1")) {
                            driverEmptyDO.dDate = text;

                        } else if (attribute.equalsIgnoreCase("O_YDPTIME1")) {
                            driverEmptyDO.dTime = text;

                        } else if (attribute.equalsIgnoreCase("O_YARRTIME1")) {
                            driverEmptyDO.aTime = text;

                        } else if (attribute.equalsIgnoreCase("O_YVEHCODE1")) {
                            driverEmptyDO.vehicleCode = text;

                        } else if (attribute.equalsIgnoreCase("O_YCARRIER1")) {
                            driverEmptyDO.vehicleCarrier = text;

                        } else if (attribute.equalsIgnoreCase("O_YFCYDES1")) {
                            driverEmptyDO.site = text;

                        } else if (attribute.equalsIgnoreCase("O_YEDNAME")) {
                            driverEmptyDO.driver = text;

                        } else if (attribute.equalsIgnoreCase("O_YETRIP")) {
                            driverEmptyDO.tripNumber = text;

                        } else if (attribute.equalsIgnoreCase("O_YNOTE")) {
                            if (text.length() > 0) {

                                driverDO.notes = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YSRCNUM")) {
                            if (text.length() > 0) {

                                driverDO.srcNumber = text;
                            }

                        }

                        else if (attribute.equalsIgnoreCase("O_YCHKIN")) {
                            if (text.length() > 0) {

                                driverDO.checkinFlag = Integer.parseInt(text);
                            }

                        }
                        text = "";


                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        if (attribute1.equalsIgnoreCase("GRP3")) {

                            driverIdMainDO.driverDOS.add(driverDO);
                        } else {
                            driverIdMainDO.driverEmptyDOS.add(driverEmptyDO);
//
                        }

                    }


                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //  ((BaseActivity)mContext).showLoader();

    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        if (onResultListener != null) {
            onResultListener.onCompleted(!result, driverIdMainDO);
        }
    }
}