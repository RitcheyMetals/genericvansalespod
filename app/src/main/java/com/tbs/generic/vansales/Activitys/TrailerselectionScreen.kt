package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.app.PendingIntent.getActivity
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.alert.rajdialogs.ProgressDialog
import com.tbs.generic.vansales.Adapters.TrailerSelectionAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.TrailerSelectionDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.TrailerSelectionRequest
import com.tbs.generic.vansales.dialogs.YesOrNoDialogFragment
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.configuration_layout.*
import kotlinx.android.synthetic.main.trailer_selection_screen.*
import kotlinx.android.synthetic.main.vr_selection_screen.tvNoData
import java.util.*


class TrailerselectionScreen : BaseActivity() {
    lateinit var adapter: TrailerSelectionAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : RecyclerView
    private var progressDialog: ProgressDialog? = null
      var selectDOS: ArrayList<TrailerSelectionDO>? = null

    override fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = layoutInflater.inflate(R.layout.trailer_selection_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()

        flToolbar.visibility = View.GONE
        ivGoBack.setOnClickListener {
            Util.preventTwoClick(it)
            finish()
        }
        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)
//
//        act_toolbar.setNavigationOnClickListener {
//            finish()
//        }
        initializeControls()
    }
    override fun initializeControls() {
        tvScreenTitles.text = getString(R.string.select_trailer_screen)

        recycleview = findViewById<RecyclerView>(R.id.recycleview)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        recycleview.layoutManager = linearLayoutManager
          VR()
//        adapter = TrailerSelectionAdapter(this@VRselectionScreen,  ArrayList())
//        recycleview.adapter = adapter
        btnConfirm.setOnClickListener {


            if(adapter!=null){
                val selectedTrailerDOs = adapter.selectedTrailerDOs;
                if(selectedTrailerDOs!=null && !selectedTrailerDOs.isEmpty()){
                    if(selectedTrailerDOs.size<3) {
                        val intent = Intent()
                        intent.putExtra("AddedProducts", selectedTrailerDOs);
                        setResult(11, intent)
                        finish()
                    }else{
                        showToast(resources.getString(R.string.max_2))

                    }
//                    val intent = Intent()
//                    intent.putExtra("AddedProducts", selectedTrailerDOs);
//                    setResult(11, intent)
//                    finish()
                }
                else{
                    showToast(resources.getString(R.string.select_items))
                }
            }
            else{
                showToast(resources.getString(R.string.no_data))
            }

        }
        ivSearchs.setOnClickListener{
            Util.preventTwoClick(it)
//            if(llSearch.visibility == View.VISIBLE){
//                llSearch.visibility = View.INVISIBLE
//            }
//            else{
//            }
            val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            etSearch.requestFocus()
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
            tvScreenTitles.visibility= View.GONE
            llSearch.visibility = View.VISIBLE
        }
        ivClearSearch.setOnClickListener{
            Util.preventTwoClick(it)
            etSearch.setText("")
            if(selectDOS!=null && selectDOS!!.size>0){
                adapter = TrailerSelectionAdapter(this@TrailerselectionScreen,  selectDOS)
                recycleview.adapter = adapter
                tvNoData.visibility = View.GONE
                recycleview.visibility = View.VISIBLE
            }
            else{
                tvNoData.visibility = View.VISIBLE
                recycleview.visibility = View.GONE
            }
            llSearch.visibility=View.GONE
            tvScreenTitles.visibility=View.VISIBLE
        }

        etSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable?) {
                if(etSearch.text.toString().equals("", true)){
                    if(selectDOS!=null &&selectDOS!!.size>0){
                        adapter = TrailerSelectionAdapter(this@TrailerselectionScreen, selectDOS)
                        recycleview.adapter = adapter
                        tvNoData.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                    }
                    else{
                        tvNoData.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                    }
                }
                else if(etSearch.text.toString().length>1){
                    filter(etSearch.text.toString())
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun filter(filtered : String) : ArrayList<TrailerSelectionDO>{
        val customerDOs = ArrayList<TrailerSelectionDO>()
        for (i in selectDOS!!.indices){
            if(selectDOS!!.get(i).trailer.contains(filtered, true)
                    ||selectDOS!!.get(i).trailerDescription.contains(filtered, true)){
                customerDOs.add(selectDOS!!.get(i))
            }
        }
        if(customerDOs.size>0){
            adapter.refreshAdapter(customerDOs)
            tvNoData.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        }
        else{
            tvNoData.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return customerDOs
    }

    override fun onBackPressed() {
        if (!svSearch.isIconified) {
            svSearch.isIconified = true
            return
        }
        super.onBackPressed()
    }
    fun VR(){
        if (Util.isNetworkAvailable(this)) {
            progressDialog?.show()
            val driverListRequest = TrailerSelectionRequest( this)
            driverListRequest.setOnResultListener { isError, modelDO ->
                //hideLoader()
                if (isError) {
                    progressDialog?.hide()
                    btnConfirm.visibility=View.GONE

                    showAlert(getString(R.string.server_error))
                } else {
                    progressDialog?.hide()


                    selectDOS=modelDO.trailerSelectionDOS
                    if(selectDOS!=null&& selectDOS!!.size>0){
                        adapter = TrailerSelectionAdapter(this@TrailerselectionScreen,  selectDOS)
                        recycleview.adapter = adapter
                        tvNoData.visibility=View.GONE
                        btnConfirm.visibility=View.VISIBLE

                    }else{
                        btnConfirm.visibility=View.GONE

                        showToast(getString(R.string.no_data_found))
                        tvNoData.visibility=View.VISIBLE
                    }

                }
            }
            driverListRequest.execute()
        } else {
            YesOrNoDialogFragment().newInstance(getString(R.string.alert), getString(R.string.internet_connection), ResultListner { `object`, isSuccess -> }, false).show((this as AppCompatActivity).supportFragmentManager, "YesOrNoDialogFragment")
        }
    }
}